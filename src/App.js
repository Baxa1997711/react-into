import React, { useState, useEffect } from "react";
import "./App.css";
import Header from "./Components/Header";
import ProductList from "./Components/ProductsList/ProductList";
import axios from 'axios'

const App = () => {
  const [posts, setPosts] = useState([])
  const [allPost, setAllPost] = useState(false)
  
  
  const getPosts = () => {
    axios.get('https://jsonplaceholder.typicode.com/posts')
    .then(res => {
      console.log('res', res);
      setPosts(res.data)
    })
  }

  console.log('allpost', allPost);

  useEffect(() => {
    getPosts()
  }, [])
  

  const products = [
    {
      id: 1,
      title: "Red Converse",
      image:
        "https://images.unsplash.com/photo-1607522370275-f14206abe5d3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2021&q=80",
      price: 35,
      count: 3,
    },
    {
      id: 2,
      title: "Nike Air",
      image:
        "https://images.unsplash.com/photo-1549298916-b41d501d3772?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2012&q=80",
      price: 50,
      count: 0,
    },
    {
      id: 3,
      title: "Puma White Sneakers",
      image:
        "https://images.unsplash.com/photo-1608231387042-66d1773070a5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1974&q=80",
      price: 25,
      count: 1,
    },
    {
      id: 4,
      title: "Red Converse",
      image:
        "https://images.unsplash.com/photo-1607522370275-f14206abe5d3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2021&q=80",
      price: 35,
      count: 3,
    },
    {
      id: 5,
      title: "Nike Air",
      image:
        "https://images.unsplash.com/photo-1549298916-b41d501d3772?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2012&q=80",
      price: 50,
      count: 0,
    },
    {
      id: 6,
      title: "Puma White Sneakers",
      image:
        "https://images.unsplash.com/photo-1608231387042-66d1773070a5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1974&q=80",
      price: 25,
      count: 1,
    },
    {
      id: 7,
      title: "Red Converse",
      image:
        "https://images.unsplash.com/photo-1607522370275-f14206abe5d3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2021&q=80",
      price: 35,
      count: 3,
    },
    {
      id: 8,
      title: "Nike Air",
      image:
        "https://images.unsplash.com/photo-1549298916-b41d501d3772?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2012&q=80",
      price: 50,
      count: 0,
    },
    {
      id: 9,
      title: "Puma White Sneakers",
      image:
        "https://images.unsplash.com/photo-1608231387042-66d1773070a5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1974&q=80",
      price: 25,
      count: 1,
    },
  ];

  const limitedPost = posts.slice(0, 10).map(post => (
    <div style={{margin: '10px', border: '1px solid #000', padding: '5px'}} key={post.id}>
      <h1>{post.title}</h1>
      <p>{post.body}</p>
    </div>
  ))

  const allPosts = posts.map(post => (
    <div style={{margin: '10px', border: '1px solid #000', padding: '5px'}} key={post.id}>
      <h1>{post.title}</h1>
      <p>{post.body}</p>
    </div>
  ))

  return (
    <>
      <Header/>
      <ProductList title={"Best Products"} products={products}/>
      <ProductList title={"Products"} products={products.filter(product => product.count !== 0)}/>
      <h1>POSTS</h1>
      {allPost ? allPosts : limitedPost}
      <button style={{margin: '10px', padding: '10px'}} onClick={() => setAllPost(!allPost)}>{allPost ? "Show Less Posts" : "Show All Posts"}</button>
    </>
  )
}
export default App;
